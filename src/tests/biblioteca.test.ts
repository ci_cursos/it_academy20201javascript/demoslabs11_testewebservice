import mongoose from 'mongoose';
import * as dbhandler from './dbhandler';
import { Biblioteca } from '../negocio/biblioteca';
import { AutorModel } from '../persistencia/autorModel';
import { LivroModel } from '../persistencia/livroModel';
import { LivroRepositorio } from '../persistencia/livroRepositorio';
import { Livro } from '../entidades/livro';
import { Emprestimo } from '../entidades/emprestimo';
import { EmprestimoRepositorio } from '../persistencia/emprestimoRepositorio';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Recriar dados de teste no banco antes de cada teste.
 */
beforeEach(async () => {
    await seedDatabase();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('Biblioteca', () => {
    describe('buscarTodosLivros() integração', () => {
        test('deve retornar uma coleção com um livro', async () => {
            const resultLivros = await Biblioteca.buscarTodosLivros();
            expect(resultLivros).toBeDefined();
            expect(resultLivros).toHaveLength(1);
            expect(resultLivros[0].titulo).toEqual('Teste');
        });
    })
    describe('emprestarLivro() unitário', () => {
        test('deve retornar empréstimo do livro', async () => {
            //Criar mock para a função Date.now()
            const NOW = '2020-07-20T08:00:00.000Z';
            const NOWMAIS7DIAS = '2020-07-27T08:00:00.000Z';
            const mockDateNow = jest.spyOn(global.Date, 'now').mockImplementation(() => new Date(NOW).getTime());
            
            //Criar mock para a função LivroRepositorio.buscarPorId
            const mockLivro: Livro = {
                titulo: 'Teste',
                autores: [{
                    primeiro_nome: 'John',
                    ultimo_nome: 'Doe'
                }]
            }; 
            const mockLivroRepositorioBuscarPorId = jest
                .spyOn(LivroRepositorio, 'buscarPorId')
                .mockResolvedValue(mockLivro);

            //Criar mock para a função EmprestimoRepositorio.criar
            const mockEmprestimo: Emprestimo = {
                livro: mockLivro,
                dataEntrega: new Date(NOWMAIS7DIAS),
                dataRetirada: new Date(NOW)
            };
            const mockEmprestimoRepositorioCriar = jest
                .spyOn(EmprestimoRepositorio,'criar')
                .mockResolvedValue(mockEmprestimo);

            const resultEmprestimo = await Biblioteca.emprestarLivro('id');
            expect(mockLivroRepositorioBuscarPorId).toHaveBeenCalledWith('id');
            expect(mockEmprestimoRepositorioCriar).toHaveBeenCalledWith(mockEmprestimo);
            expect(resultEmprestimo).toBeDefined();
            expect(resultEmprestimo.livro.titulo).toEqual(mockLivro.titulo);
            expect(resultEmprestimo.livro.autores).toEqual(mockLivro.autores);
            expect(resultEmprestimo.dataRetirada).toEqual(new Date(NOW));
            expect(resultEmprestimo.dataEntrega).toEqual(new Date(NOWMAIS7DIAS));
            
            //Desfazer os mocks
            mockDateNow.mockRestore();
            mockLivroRepositorioBuscarPorId.mockRestore();
            mockEmprestimoRepositorioCriar.mockRestore();
        });
    });
});

async function seedDatabase() {
    const a1 = await AutorModel.create({
        primeiro_nome: 'John',
        ultimo_nome: 'Doe'
    });
    const a2 = await AutorModel.create({
        primeiro_nome: 'Mary',
        ultimo_nome: 'Doe'
    });
    await LivroModel.create({
        titulo: 'Teste',
        autores: [a1]
    });
}
