import mongoose from 'mongoose';
import request from 'supertest';
import * as dbhandler from './dbhandler';
import { AutorModel } from '../persistencia/autorModel';
import { LivroModel } from '../persistencia/livroModel';
import { Biblioteca } from '../negocio/biblioteca';
import app from '../app';
import { Livro } from '../entidades/livro';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

async function seedDatabase() {
    const a1 = await AutorModel.create({
        primeiro_nome: 'John',
        ultimo_nome: 'Doe'
    });
    const a2 = await AutorModel.create({
        primeiro_nome: 'Mary',
        ultimo_nome: 'Doe'
    });
    const l1 = await LivroModel.create({
        titulo: 'Teste',
        autores: [a1]
    });
}

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Recriar dados de teste no banco antes de cada teste.
 */
beforeEach(async () => {
    await seedDatabase();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('Biblioteca web service', () => {
    describe('GET /api/livros/:id unitário', () => {
        test('deve retornar um livro com o id indicado', async () => {
            //Criar mock para a função Biblioteca.buscarLivroPorId
            const mockLivro: Livro = {
                titulo: 'Teste',
                autores: [{
                    primeiro_nome: 'John',
                    ultimo_nome: 'Doe'
                }]
            }; 
            const mockBibliotecaBuscarLivroPorId = jest
                .spyOn(Biblioteca, 'buscarLivroPorId')
                .mockResolvedValue(mockLivro);

            const result = await request(app).get('/api/livros/id');
            expect(mockBibliotecaBuscarLivroPorId).toHaveBeenCalledWith('id');
            expect(result.status).toBe(200);
            expect(result.body.titulo).toEqual(mockLivro.titulo);
            expect(result.body.autores).toEqual(mockLivro.autores);
            
            //Desfazer os mocks
            mockBibliotecaBuscarLivroPorId.mockReset();
        });
    });
});