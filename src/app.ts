import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import bodyParser from 'body-parser';
import errorHandler from 'errorhandler';
import { router } from './routers/biblioteca.routes';

//Configuração middleware do Express
const app = express();
app.set('port', process.env.PORT);
app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
if (process.env.NODE_ENV === 'development') {
    app.use(errorHandler());
}

//Adição das rotas
app.use('/api',router);

export default app;
